package com.gpo.backend.dto.person;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PersonDto {
    private int id;

    private String name;

    private String email;
}

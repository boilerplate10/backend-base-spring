package com.gpo.backend.dto.person;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class UpdatePersonInputDto {
    @Nullable
    @Size(min = 3, max = 20)
    private String name;

    @Nullable
    @Email
    private String email;

    @Nullable
    @Size(min = 8, max = 24)
    private String password;
}
package com.gpo.backend.dto.person;

import lombok.*;

import javax.validation.constraints.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CreatePersonInputDto {
    @NotEmpty
    @Size(min = 3, max = 20)
    private String name;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(min = 8, max = 24)
    private String password;
}

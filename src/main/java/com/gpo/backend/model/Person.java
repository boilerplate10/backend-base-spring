package com.gpo.backend.model;

import com.gpo.backend.dto.person.PersonDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "person")
public class Person {
    @GeneratedValue
    @Id
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    public PersonDto asDto() {
        return new PersonDto(
                this.getId(),
                this.getName(),
                this.getEmail()
        );
    }
}

package com.gpo.backend.controller;

import com.gpo.backend.dto.person.CreatePersonInputDto;
import com.gpo.backend.dto.person.PersonDto;
import com.gpo.backend.dto.person.UpdatePersonInputDto;
import com.gpo.backend.service.PersonService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> save(@Valid @RequestBody CreatePersonInputDto person) {
        PersonDto createdPerson = personService.create(person);
        return new ResponseEntity<>(createdPerson, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<PersonDto>> findAll() {
        List<PersonDto> personDtoList = personService.findAll();
        return new ResponseEntity<>(personDtoList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> findById(@PathVariable Integer id) {
        PersonDto personDto = personService.findById(id);
        if (personDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(personDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id) {
        try {
            personService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<PersonDto> update(
            @PathVariable Integer id,
            @Valid @RequestBody UpdatePersonInputDto person
    ) {
        PersonDto personDto = personService.update(id, person);
        if (personDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(personDto, HttpStatus.OK);
    }
}

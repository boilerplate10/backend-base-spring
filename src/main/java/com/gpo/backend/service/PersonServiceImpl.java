package com.gpo.backend.service;

import com.gpo.backend.dto.person.CreatePersonInputDto;
import com.gpo.backend.dto.person.PersonDto;
import com.gpo.backend.dto.person.UpdatePersonInputDto;
import com.gpo.backend.model.Person;
import com.gpo.backend.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public PersonDto create(CreatePersonInputDto personDto) {
        LOGGER.info(String.format("create - payload: [%s]", personDto.toString()));

        // Convert DTO to Entity
        ModelMapper modelMapper = new ModelMapper();
        Person person = modelMapper.map(personDto, Person.class);

        person = personRepository.save(person);

        return person.asDto();
    }

    @Override
    public PersonDto update(Integer id, UpdatePersonInputDto personDto) {
        LOGGER.info(String.format("update - id: [%d] payload: [%s]", id, personDto.toString()));
        Optional<Person> personOpt = personRepository.findById(id);
        if (personOpt.isPresent()) {
            Person person = personOpt.get();
            if (personDto.getEmail() != null) person.setEmail(personDto.getEmail());
            if (personDto.getName() != null) person.setName(personDto.getName());
            if (personDto.getPassword() != null) person.setPassword(personDto.getPassword());

            person = personRepository.save(person);

            return person.asDto();
        }
        return null;
    }

    @Override
    public List<PersonDto> findAll() {
        LOGGER.info("findAll");
        return personRepository.findAll()
                .stream()
                .map(p -> p.asDto())
                .collect(Collectors.toList());
    }

    @Override
    public PersonDto findById(Integer id) {
        LOGGER.info(String.format("findById - id: [%d]", id));
        Optional<Person> personOpt = personRepository.findById(id);
        if (personOpt.isPresent()) {
            Person person = personOpt.get();
            return person.asDto();
        }
        return null;
    }

    @Override
    public void deleteById(Integer id) throws EmptyResultDataAccessException {
        LOGGER.info(String.format("deleteById - id: [%d]", id));
        personRepository.deleteById(id);
    }
}

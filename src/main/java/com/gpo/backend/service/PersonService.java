package com.gpo.backend.service;

import com.gpo.backend.dto.person.CreatePersonInputDto;
import com.gpo.backend.dto.person.PersonDto;
import com.gpo.backend.dto.person.UpdatePersonInputDto;

import java.util.List;

public interface PersonService {
    public PersonDto create(CreatePersonInputDto personDto);
    public PersonDto update(Integer id, UpdatePersonInputDto personDto);
    public List<PersonDto> findAll();
    public PersonDto findById(Integer id);
    public void deleteById(Integer id);
}
